
AjaxSolr.TagcloudWidget = AjaxSolr.AbstractFacetWidget.extend({
    afterRequest: function () {
    var self = this;
    var self_target = $(self.target); 
      if (self.manager.response.facet_counts.facet_fields[self.field] === undefined) {
        self_target.html('no items found in current selection');
        return;
      }

      var maxCount = 0;
      var objectedItems = [];
      for (var facet in self.manager.response.facet_counts.facet_fields[self.field]) {
        var count = parseInt(self.manager.response.facet_counts.facet_fields[self.field][facet]);
        if (count > maxCount) {
          maxCount = count;
        }
        objectedItems.push({ facet: facet, count: count });
      }
      objectedItems.sort(function (a, b) {
        return a.facet < b.facet ? -1 : 1;
      });

      self_target.empty();
      for (var i = 0, l = objectedItems.length; i < l; i++) {
        var facet = objectedItems[i].facet;
        self_target.append(
          $('<span class="tagcloud_item"></span>')
          .text(facet + ' (' + objectedItems[i].count + ')')
          .click(self.clickHandler(facet))
        );
      }
    }
  });
