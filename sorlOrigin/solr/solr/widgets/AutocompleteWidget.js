AjaxSolr.AutocompleteWidget = AjaxSolr.AbstractTextWidget.extend({
    afterRequest: function () {

	var the_input = $(this.target).find('input');

	var org_value = the_input.val();

	the_input.unbind().removeData('events').val('');

	the_input.val(org_value);

	var self = this;

	var callback = function (response) {

	    //console.log('Response ', response);

	    var list = [];
	    for (var i = 0; i < self.fields.length; i++) {
		var field = self.fields[i];
		for (var facet in response.facet_counts.facet_fields[field]) {
		    list.push({
			field: field,
			value: facet,
			label: facet + ' (' + response.facet_counts.facet_fields[field][facet] + ') - ' + field
		    });
		}
	    }

	    self.requestSent = false;
	    the_input.autocomplete('destroy').autocomplete({
		source: list,
		select: function(event, ui) {
		    if (ui.item) {
			self.requestSent = true;
			if (self.manager.store.addByValue('fq', ui.item.field + ':' + AjaxSolr.Parameter.escapeValue(ui.item.value))) {
			    //console.log('Sending autocomplete request for ', ui);
			    self.doRequest();
			}
		    }
		}
	    });

	    // This has lower priority so that requestSent is set.
	    the_input.bind('keydown', function(e) {
		if (self.requestSent === false && e.which == 13) {
		    var value = $(this).val();
		    if (value && self.set(value)) {
			//console.log('Making request for ', value);
			self.doRequest();
		    }
		}
	    });
	} // end callback

	var params = [ 'rows=0&facet=true&facet.limit=-1&facet.mincount=1&json.nl=map' ];
	for (var i = 0; i < this.fields.length; i++) {
	    params.push(`facet.field=${this.fields[i]}`);
	}
	var values = this.manager.store.values('fq');
	for (var i = 0; i < values.length; i++) {
	    params.push('fq=' + encodeURIComponent(values[i]));
	}
	params.push('q=' + this.manager.store.get('q').val());
	params.push('wt=json');
	// console.log('Contacting ', this.manager.solrUrl);
	// console.log('with data ', params);
	$.getJSON(this.manager.solrUrl, params.join('&'), callback);

    }
});
