(function ($) {
    AjaxSolr.DataTableWidget = AjaxSolr.AbstractWidget.extend({
        currentColumns: [],
        currentData: [[]],
        init: function () {
            Mfields = "id,_text_,type,url,title,description,dataset_type,dataset_technology,samples,repository,is_in_beacon,file_formats,edited_time,dataset_studies,_version_";
            var self = this; // So we can use the this reference from inner function

            this.currentColumns = [];
            $.each(Mfields.split(","), function (key, value) {
                self.currentColumns[self.currentColumns.length] = { "sTitle": value };
            });

            //Initialize data
            var table = $(this.target).DataTable({
                "bJQueryUI": true,
                "pagingType": "full_numbers_no_ellipses",
                "dom": "tr" +
                    "<'row'<'col-xs-6'l><'col-xs-6'p>>",
                "drawCallback": function (settings, row, aoData) {
                    var api = this.api();
                    $('#pager-header').html(settings._iRecordsTotal);

                    $('#example3_paginate a').removeClass("paginate_button");
                    $('#example3_paginate a').addClass("pagination-sm");

                    $('table.dataTable thead th:eq(1)').html("Title and Description");
                    $('table.dataTable thead th:eq(2)').html("Type");

                    var i = settings.aaSorting[0][0]
                    if (parseInt(i) == 0) {
                        $('table.dataTable thead .sorting_asc').html("ID <i class='fa fa-angle-up fa-fw'> </i>");
                        $('table.dataTable thead .sorting_desc').html("ID <i class='fa fa-angle-down fa-fw'> </i>");

                    } else if (parseInt(i) == 1) {
                        $('table.dataTable thead .sorting_asc').html("Title and Description <i class='fa fa-angle-up fa-fw'> </i>");
                        $('table.dataTable thead .sorting_desc').html("Title and Description <i class='fa fa-angle-down fa-fw'> </i>");
                    } else {
                        $('table.dataTable thead .sorting_asc').html("Type <i class='fa fa-angle-up fa-fw'> </i>");
                        $('table.dataTable thead .sorting_desc').html("Type <i class='fa fa-angle-down fa-fw'> </i>");
                    }

                    
                    $('#articlediv').addClass("double-border");

                    $(".show-more").on("click", function () {
                        
                        $(this).parent().parent().children().first().css("display", "none");
                        $(this).parent().parent().children().last().css("display", "block");

                    });

                    $(".show-less").on("click", function () {
                       
                        $(this).parent().parent().children().first().css("display", "block");
                        $(this).parent().parent().children().last().css("display", "none");

                    });


                    // Output the data for the visible rows to the browser's console
                    //console.log(api.rows({ page: 'current' }).data());
                },
                "rowCallback": function (row, aoData) {

                    var id = '<a href="' + aoData[3] + '" target="_blank" >' + aoData[0] + '</a>'

                    if (aoData[5][0].length > 300) {

                        var description = aoData[5][0]
                        var title = aoData[4][0]
                        if (description.includes("<p>")) {
                            var des = description.split("<p>").join(" ").split("</p>").join(" ").split("<\n>").join(" ");

                            if (title.includes("_")) {
                                
                                var descriptionShow = des.toString().substring(0, 300) + "..." + '<a class="button show-more" >⊕</a>';
                                var descriptionLess = des + '<a class="button show-less" > ⊖ </a>';
                                var t = title.split("_").join(" ");
                                var td = '<strong>' + t + '</strong>' + '<br>' + '<ul class="myList" style="list-style: none;">' + '<li>' + descriptionShow + '</li>' + '<li style="display: none;" >' + descriptionLess + '</li>' + '</ul>'
                                $('td:eq(1)', row).html(td);
                                $('td:eq(0)', row).html(id);

                            } else {
                                
                                var descriptionShow = des.toString().substring(0, 300) + "..." + '<a class="button show-more" >⊕</a>';
                                var descriptionLess = des + '<a class="button show-less" > ⊖ </a>';
                                var t = title;
                                var td = '<strong>' + t + '</strong>' + '<br>' + '<ul class="myList" style="list-style: none;">' + '<li>' + descriptionShow + '</li>' + '<li style="display: none;" >' + descriptionLess + '</li>' + '</ul>'
                                $('td:eq(1)', row).html(td);
                                $('td:eq(0)', row).html(id);
                            }

                        } else {
                            if (title.includes("_")) {
                                
                                var descriptionShow = aoData[5][0].toString().substring(0, 300) + "..." + '<a class="button show-more" >⊕</a>';
                                var descriptionLess = aoData[5][0] + '<a class="button show-less" > ⊖ </a>';
                                var t = title.split("_").join(" ");
                                var td = '<strong>' + t + '</strong>' + '<br>' + '<ul class="myList" style="list-style: none;">' + '<li>' + descriptionShow + '</li>' + '<li style="display: none;" >' + descriptionLess + '</li>' + '</ul>'
                                $('td:eq(1)', row).html(td);
                                $('td:eq(0)', row).html(id);

                            } else {
                               
                                var descriptionShow = aoData[5][0].toString().substring(0, 300) + "..." + '<a class="button show-more" >⊕</a>';
                                var descriptionLess = aoData[5][0] + '<a class="button show-less" > ⊖ </a>';
                                var t = title;
                                var td = '<strong>' + t + '</strong>' + '<br>' + '<ul class="myList" style="list-style: none;">' + '<li>' + descriptionShow + '</li>' + '<li style="display: none;" >' + descriptionLess + '</li>' + '</ul>'
                                $('td:eq(1)', row).html(td);
                                $('td:eq(0)', row).html(id);
                            }
                        }

                    } else {
                        var descriptionShow = aoData[5][0];
                        var title = aoData[4][0]
                        if (title.includes("_")) {
                            var t = title.split("_").join(" ");
                            var td = '<strong>' + t + '</strong>' + '<br>' + '<p>' + descriptionShow + '</p>'
                            $('td:eq(1)', row).html(td);
                            $('td:eq(0)', row).html(id);

                        } else {
                            var t = title;
                            var td = '<strong>' + t + '</strong>' + '<br>' + '<p>' + descriptionShow + '</p>'
                            $('td:eq(1)', row).html(td);
                            $('td:eq(0)', row).html(id);
                        }
                    }


                },
                "bLengthChange": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": null,
                "fnServerData": function (sSource, aoData, fnCallback, settings) {
                    // Rownumber

                    self.manager.store.addByValue('rows', settings._iDisplayLength);
                    // Filtering

                    self.manager.store.addByValue('q', '*:*');
                    // Fields
                    self.manager.store.addByValue('fl', Mfields);

                    self.manager.store.addByValue('start', parseInt(settings._iDisplayStart));

                    self.manager.doRequest();
                    self.manager.setCallback(this, fnCallback);
                }
                ,
                "aoColumns": [
                    { aoData: "id" },
                    {
                        aoData: "Title and Description",
                        "defaultContent": ""
                    }
                ]
            });

        }
    });

})(jQuery);