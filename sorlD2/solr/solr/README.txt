# Solr Facet Web
In this webpage you can search with facets a given characteristics of the metadata of all the objects available in the EGA.
### Adding new facets:
* For a field with its checkboxes, put the name of the field in the list "all_fields" in **js/reuters.7.js**.
* If you want another range field, like the one in the "Samples" facet, you need to put the name of the field in "range_fields" of **js/reuters.7.js**.
* Add the facet front-end in the **index.html**. As the template below:
```html
<div  class="NameOfTheField-class">
	<h2>Name of the field</h2>
	<div  class="tagcloud"  id="NameOfTheField"></div>
</div>
```

Online site: http://tf.crg.eu:10060/
This web is a modification of [Ajax-Solr Evolving Web](https://github.com/evolvingweb/ajax-solr) and [Ajax-Solr Extensions](https://github.com/buddyroo30/Ajax-Solr-Extensions).

