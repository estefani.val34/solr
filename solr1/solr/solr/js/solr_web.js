(function () { //scoping

	var Manager;

	Manager = new AjaxSolr.Manager({
		servlet: '',
		solrUrl: 'https://web2.ega-archive.org/solr-proxy'
	});

	/*
	Antes de definir cualquier método en el widget, agreguemos 
	una instancia del widget al Administrador en reuters.js:
	*/

	Manager.addWidget(new AjaxSolr.ResultWidget({
		id: 'result',
		target: '#docs',
		highlighting: true, //set to true to show contextual, highlighted snippets (from Solr highlighting); will also need to add highlighting params (below)
		no_init_results: true //set true to NOT show full result set for init query *:*
		

	}));

	/*
	JAX Solr viene con un buen widget de buscapersonas jQuery, 
	basado en el complemento will_paginate para Ruby on Rails,
	por lo que solo necesitamos anular algunas de sus 
	propiedades y métodos para obtener la funcionalidad que
	queremos.
	Además de la identificación básica y las propiedades 
	de destino, PagerWidget expone algunas de sus propias 
	propiedades. Implementamos el método abstracto renderHeader
	 para mostrar la cantidad de resultados encontrados.

	En esta iteración, mostramos cómo implementar un widget
	anulando las propiedades de un widget existente. 
	Si estuviéramos agregando una cantidad significativa 
	de funcionalidad, deberíamos haber creado un nuevo widget, 
	como hicimos con el widget de resultados. En este caso, 
	teníamos poco código para escribir, por lo que tiene sentido
	anular las propiedades del widget "pager" e 
	implementar sus métodos abstractos durante 
	la creación de instancias.

	Manager.addWidget(new AjaxSolr.PagerWidget({
		id: 'pager',
		target: '#pager',
		no_init_results: true,
		prevLabel: 'Previous',
		nextLabel: 'Next',
		innerWindow: 1,
		
	}));*/

	// Manager.addWidget(new AjaxSolr.PagerWidget({
	// 	id: 'pager2',
	// 	target: '#pager2',
	// 	no_init_results: true,
	// 	prevLabel: 'Previous',
	// 	nextLabel: 'Next',
	// 	innerWindow: 1,

	// 	renderHeader: function (perPage, offset, total) {
	// 		varrecords=  '<div class="col-xs-6">'+
	// 						'<div class="dataTables_length">'+
	// 		  					'<ul id="length-select">'+
	// 								'<li class="change-length" data-length="1000000" >All</li>'+
	// 								'<li class="change-length" data-length="10" >10</li>'+
	// 								'<li class="change-length" data-length="25" >25</li>'+
	// 								'<li class="change-length" data-length="50" >50</li>'+
	// 								'<li class="change-length" data-length="100" >100</li>'+
	// 								'<p>records per page</p>'+
	// 		  					'</ul>'+
	// 						'</div>'+
	// 	  				'</div>'


	// 		varhtml='<span id="inforesults"> '+
	// 					'<i class="fa fa-folder-open fa-fw" id="iconoresults"> </i>'+
	// 				' Total number of results: </span>'+
	// 				'<span id="numresults">'+ ` ${total}`+ ` ${perPage}`+ ` ${offset} `+'</span>';
			
	// 		$('#pager-header').html(varhtml);
	// 		$('#records').html(varrecords);
			
	// 		//console.log(">>>")
	// 		//console.log(this.windowedLinks())
			
	// 		// var rmanager = this.manager;
	// 		this.manager.store.params.rows.value = 20;
	// 		// var rowsvalue = this.manager.store.params.rows.value 
			
	// 		// $("#length-select li").click(function () {
	// 		// 	//console.log($(this).data().length)
	// 		// 	//console.log($(this).data().length == rowsvalue )
	// 		// 	if ($(this).data().length == 100 ) {
	// 		// 		rmanager.store.params.rows.value = 100;
	// 		// 	}
	// 		// 	console.log(rmanager.store.params.rows.value);
	// 		// });
	// 	},

	// }));

	var all_fields = ['type',
		'repository',
		'study_type',
		'dataset_type',
		'dataset_technology',
		'phenotypes',
		'file_formats',
		'is_in_beacon',
		'DUO',
		'domain'];

	// To retrieve the facets the TagCloudWidget and the MultiSelectWidget can be used.
	// MultiSelectWidget is used for the sorted facets and due to the selection is done by checkboxes. 
    /*
    for (var i = 0, l = fields.length; i < l; i++) {
	Manager.addWidget(new AjaxSolr.TagcloudWidget({
	    id: fields[i],
	    target: '#' + fields[i],
	    field: fields[i]
	}));
    }
    */

	for (var i = 0, l = all_fields.length; i < l; i++) {
		Manager.addWidget(new AjaxSolr.MultiSelectWidget({ //MultiSelectWidget instead of Tagcloudwidget
			id: all_fields[i],
			target: '#' + all_fields[i],
			field: all_fields[i],
			//max_show: 10,
			//max_facets: 20,
		}));
	}

	Manager.addWidget(new AjaxSolr.CurrentSearchWidget({
		id: 'currentsearch',
		target: '#selection'
	}));

	Manager.addWidget(new AjaxSolr.AutocompleteWidget({
	 	id: 'text',
	 	target: '#search',
	 	fields: all_fields
	}));

	var range_fields = ['samples'];
	for (var i = 0, l = range_fields.length; i < l; i++) {
		Manager.addWidget(new AjaxSolr.RangeWidget({
			id: range_fields[i], //Same as for TagcloudWidget.js
			target: '#' + range_fields[i], //Same as for TagcloudWidget.js
			field: range_fields[i] //Same as for TagcloudWidget.js
		}));
	}

	Manager.init(); //para inicializar AJAX Solr. Este método llama a los métodos init de ParameterStore y widgets:

	Manager.store.addByValue('q', '*:*');
	var params = {
		rows : 0,
		stats: true,
		'stats.field': range_fields,
		facet: true,
		'facet.field': all_fields,
		//'facet.limit': 20,
		'facet.mincount': 1,
		//'f.topics.facet.limit': 50,
		'json.nl': 'map',
		//If highlighting is set to true for ResultWidget above, need to add these 3 params:
		'hl': true,
		'hl.fl': 'description', //The field for which you want highlighting snippets
		'hl.snippets': 4, //Change if you want more or less highlighting snippets
		//Also for highlighting, can optionally set these params for how you want the highlighting to look (yellow background here; Solr default is <em>...</em>):
		'hl.simple.pre': '<span class="hilite">',
		'hl.simple.post': '</span>'
	};
	for (var name in params) {
		if (params.hasOwnProperty(name)) {
			Manager.store.addByValue(name, params[name]);
		}
	}
	Manager.doRequest(); // Después de inicializar AJAX Solr, normalmente se llama al método doRequest del administrador, para enviar la primera solicitud a Solr:

})(); // end of scoping


