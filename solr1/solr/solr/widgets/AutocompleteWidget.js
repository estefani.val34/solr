/*
Ahora reemplazaremos el widget de texto libre que creamos
en el último 
paso con un widget que también puede completar automáticamente 
los valores de facetas de los campos de facetas de temas,
organizaciones e intercambios.

Cree un nuevo widget, AutocompleteWidget.js, heredando de
AbstractTextWidget, como en el último paso:

Nota: Si está utilizando su propia instancia de Solr,
 es posible que desee cambiar los valores asignados 
 a las teclas de campo.

El widget de autocompletar tomará un parámetro de campos
 personalizados, enumerando los campos de faceta en los 
 que se realizará la autocompletación. Al no codificar estos
  campos de facetas, hacemos que el widget sea reutilizable.

Implemente el método abstracto afterRequest

Nuevamente estamos usando el parámetro AddByValue
de ParameterStore y establecemos métodos API, 
como lo hicimos en los resultados y los widgets de texto libre,
respectivamente. El resto es la implementación del 
complemento de Autocompletar de la interfaz de usuario jQuery.
Nota técnica: Debemos autocompletar ("destruir") para evitar
una pérdida de memoria. autocompletar ("destruir") elimina
los enlaces de teclas, por lo que debemos vincular las
teclas en afterRequest en lugar de init. sin embargo,
autocompletar ("destruir") no elimina todos los enlaces,
por lo que debemos llamar a unbind y 
removeData ("eventos") para evitar tener enlaces duplicados.

Una limitación es que solo completará automáticamente 
los valores de faceta que se encuentran dentro del límite de
 faceta para los campos de faceta dados. Por ejemplo, solo
  completará automáticamente veinte de los valores de faceta 
  en el campo faceta de intercambios, porque establecemos
   facet.limit en 20 en reuters.js. Si queremos completar
	automáticamente todos los valores de faceta, tendremos
	 que recuperarlos en una solicitud Solr separada. 
(A partir de Solr 1.5, podríamos recuperar todo en una 
	sola solicitud).

Primero, ajuste el código después de var self = this; 
en una función de devolución de llamada, reemplazando 
todas las apariciones de esto, en las que esto se
 refiere al widget, por sí mismo, porque esto no se
  referirá a la instancia del widget dentro de la función de
   devolución de llamada. Luego, reemplace todas las ocurrencias
	de this.manager.response con respuesta. La función de
	 devolución de llamada tomará la respuesta como un argumento
	 , que contendrá la respuesta de Solr que solicitaremos en
	  un momento.

*/


AjaxSolr.AutocompleteWidget = AjaxSolr.AbstractTextWidget.extend({
    afterRequest: function () {

	var the_input = $(this.target).find('input');

	var org_value = the_input.val();

	the_input.unbind().removeData('events').val('');

	the_input.val(org_value);

	var self = this;

	var callback = function (response) {

	    //console.log('Response ', response);

	    var list = [];
	    for (var i = 0; i < self.fields.length; i++) {
		var field = self.fields[i];
		for (var facet in response.facet_counts.facet_fields[field]) {
		    list.push({
			field: field,
			value: facet,
			label: facet + ' (' + response.facet_counts.facet_fields[field][facet] + ') - ' + field
		    });
		}
	    }

	    self.requestSent = false;
	    the_input.autocomplete('destroy').autocomplete({
		source: list,
		select: function(event, ui) {
		    if (ui.item) {
			self.requestSent = true;
			if (self.manager.store.addByValue('fq', ui.item.field + ':' + AjaxSolr.Parameter.escapeValue(ui.item.value))) {
			    //console.log('Sending autocomplete request for ', ui);
			    self.doRequest();
			}
		    }
		}
	    });

	    // This has lower priority so that requestSent is set.
	    the_input.bind('keydown', function(e) {
		if (self.requestSent === false && e.which == 13) {
		    var value = $(this).val();
		    if (value && self.set(value)) {
			//console.log('Making request for ', value);
			self.doRequest();
		    }
		}
	    });
	} // end callback

	/*
	Antes de enviar una nueva solicitud a Solr,
	primero debemos construir la lista de parámetros para 
	enviar a Solr. Agregue el siguiente código después de la
	función de devolución de llamada:
	hemos establecido facet.limit en un valor negativo,
	de modo que Solr devuelve todos los valores de faceta.
	*/
	var params = [ 'rows=0&facet=true&facet.limit=-1&facet.mincount=1&json.nl=map' ];
	for (var i = 0; i < this.fields.length; i++) {
	    params.push(`facet.field=${this.fields[i]}`);
	}
	/*
	Probablemente desee limitar las sugerencias de autocompletar 
	de acuerdo con los filtros y consultas actuales. 
	
	*/
	var values = this.manager.store.values('fq');
	for (var i = 0; i < values.length; i++) {
	    params.push('fq=' + encodeURIComponent(values[i]));
	}
	params.push('q=' + this.manager.store.get('q').val());
	params.push('wt=json');
	// console.log('Contacting ', this.manager.solrUrl);
	// console.log('with data ', params);
	//Ahora tomamos prestado algún código de managers / Manager.jquery.js
	// para enviar la solicitud a Solr:
	$.getJSON(this.manager.solrUrl, params.join('&'), callback);

    }
});
