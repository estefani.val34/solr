/*
Solr maneja las búsquedas de texto libre con un parámetro q.
Cree un nuevo widget, TextWidget.js, heredado de
AbstractTextWidget, que está diseñado para manejar el 
parámetro q:

A diferencia del widget tagcloud, no podemos usar el 
práctico método de API clickHandler en la función de
enlace jQuery, porque los eventos de enlace y clic
se comportan de manera diferente. En su lugar, usamos
el método de API set AbstractTextWidget directamente.
set devuelve verdadero si se modificó la consulta 
(si la consulta se establece en el mismo valor que antes,
devuelve falso). Aquí, 
si devuelve verdadero, el widget envía una solicitud al Solr.
*/

AjaxSolr.TextWidget = AjaxSolr.AbstractTextWidget.extend({
  init: function () {
    var self = this;
    $(this.target).find('input').bind('keydown', function (e) {
      if (e.which == 13) {
        var value = $(this).val();
        if (value && self.set(value)) {
          self.doRequest();
        }
      }
    });
  },

  afterRequest: function () {
    $(this.target).find('input').val('');
  }
});
