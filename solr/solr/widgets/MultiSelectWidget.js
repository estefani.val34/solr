/*
This is a useful alternative to Ajax-Solr's TagcloudWidget.js and enables faceted filtering similar to many popular e-commerce sites such as Amazon.com, Walmart.com, etc. (i.e. facet values in ordered lists selected by checking checkboxes). Whereas TagcloudWidget.js shows facet values as a tag cloud and users can select a single facet value (and only a single facet value) by clicking on it, MultiSelectWidget.js shows the top facet values in a list ordered by count with checkboxes that the user can check to choose facet values (and more than one facet value for a facet can be selected, resulting in an "OR" query for the selected values). In addition to showing the top (e.g. top 20) facet values and allowing them to be easily selected by checking a checkbox, it also has a Jquery UI autocomplete where the user can choose any facet value (JSONP queries to Solr are done based on the users typed text in the autocomplete). Finally, it also supports binned ranges for numerical values (e.g. "0 TO 24", "25 TO 49", etc.), allowing sorting of the facet values based on the ranges instead of the counts (which can be more intuitive).

Configuration example, showing all configuration fields:

Manager.addWidget(new AjaxSolr.MultiSelectWidget({
                                                  id: fields[i], //Same as for TagcloudWidget.js
			                          target: '#' + fields[i], //Same as for TagcloudWidget.js
			                          field: fields[i], //Same as for TagcloudWidget.js
			                          autocomplete_field: fields[i] + '_ci', //Name of Solr index field to use for autocomplete
			                          autocomplete_field_case: 'lower', //'lower' or 'upper'; must exactly match case if not defined
			                          max_show: 10, //maximum number of facet values to show before '+more' link
			                          max_facets: 20, //maximum number of facet values to show after '+more' clicked
			                          sort_type: 'count' //possible values: 'range', 'lex', 'count'
			                         }));

Notes:

This widget also allows users to search all possible facet values via a Jquery UI autocomplete. If a value for 'autocomplete_field' is given, that
field will be searched by the Jquery UI autocomplete (i.e. it will issue Solr JSONP requests against that field). This can simply be the same value
as specified for 'field', or a separate case-insensitive version of that field. 'autocomplete_field_case' should specify what the case of
'autocomplete_field' is ('lower' or 'upper'). If no value is specified for 'autocomplete_field', then the widget will simply fetch all values
of the facet field, and then filter them case-insensitively in JavaScript; this should be fine for facet fields with a small number of values, but
could be very slow for facet fields with many facet values, and 'autocomplete_field' should be specified in these cases.

The top facet values will be sorted based on the value specified for 'sort_type':

'range' will assume the facet values are ranges, e.g. like "0 TO 24", "25 TO 49", etc., and will sort them numerically based on the
left side of the range
'count' will sort them based on their count in the current result set (i.e. how many documents in the current result set have the facet value)
'lex' will sort them alphabetically
*/

AjaxSolr.MultiSelectWidget = AjaxSolr.AbstractFacetWidget.extend({
 
    checkboxChange: function(facet) {
	var self = this;

	var innerCheckboxChange = function() {
	    return(self.updateQueryDoRequest(facet,this.checked));
	};
	
	return(innerCheckboxChange);
    },

    updateQueryDoRequest: function(facet, checked_flag) {

	var self = this;
	var check_state = self.check_state;
	if (typeof check_state == 'undefined') {
	    self.check_state = {}; check_state = self.check_state;
	}
	if (checked_flag) { 
	    check_state[facet] = true; 
	} else {
	    delete check_state[facet];
	}
	var checked_facets_arr = $.map(check_state, function(v,i) {
	    return `"${i}"`;
	});
	self.manager.store.removeByValue('fq', new RegExp('^' + self.field));
	self.manager.store.removeByValue('facet.query', new RegExp('^' + self.field));
	if (checked_facets_arr.length > 0) {
	    var solr_query;
	    if (checked_facets_arr.length == 1) {
		solr_query = `${self.field} : ${checked_facets_arr[0]}`;
	    } else {
		solr_query = `${self.field} :( ${checked_facets_arr.join(" AND ")} )`;
	    }
	    self.manager.store.addByValue('fq' , solr_query);
	    //Need to do explicit facet queries for user-chosen items (facet.field queries are not necessarily
	    //returning full results each request, only up to facet.limit, and so it is possible user-chosen ones
	    //wouldn't be among top returned values so need to explicitly get their counts)
	    for (var i=0; i < checked_facets_arr.length; i++) {
		var cur_facet_query = `${self.field}:${checked_facets_arr[i]}`;
		self.manager.store.addByValue('facet.query' , cur_facet_query);
	    }
	}
	self.doRequest();
	return false;
    },

    sortFacets: function(objectedItems) {
	objectedItems.sort(function (a, b) {
	    return b.count < a.count ? -1 : 1;
	});
    },

    afterRequest: function () {
	var self = this;
	var returned_facets = self.manager.response.facet_counts.facet_fields[self.field];
	
	if (returned_facets === undefined) {
		returned_facets = {};
	}

	if (!(self.manager.store.find('fq', new RegExp(`^${self.field}`)))) { //reset --> all checks off // if are all true , !false
	    this.check_state = {};
	}
	
	if (typeof self.display_style == 'undefined') {
		self.display_style = 'none';
	}
	
	var checked_objectedItems = [];
	var unchecked_objectedItems = [];
	var cur_facets_hash = {};
	
	
	for (var facet in returned_facets) {
		var count = parseInt(returned_facets[facet]);
	    var facet_rec = { facet: facet, count: count };
	    if (self.check_state && self.check_state[facet]) {
			checked_objectedItems.push(facet_rec);
	    } else {
			unchecked_objectedItems.push(facet_rec);
	    }
	    cur_facets_hash[facet] = facet_rec;
	}
	
	if (typeof self.check_state != 'undefined') {
		var num_checked_facets = Object.keys(self.check_state).length;
	    if (num_checked_facets > checked_objectedItems.length) { //some checked items not present in current result set, need to add them from full result set
			for (var cur_checked_facet in self.check_state) {
				if (!cur_facets_hash[cur_checked_facet]) { //Add a new record, getting count from facet query done for it)
					var new_facet_rec = { facet: cur_checked_facet };
					new_facet_rec.count = parseInt(self.manager.response.facet_counts.facet_queries[`${self.field}:"${cur_checked_facet}"`]);
					if (typeof new_facet_rec.count == 'undefined') {
						new_facet_rec.count = 0;
					} //if for some strange reason no facet query value...
					checked_objectedItems.push(new_facet_rec);
					cur_facets_hash[cur_checked_facet] = new_facet_rec;
				}
			}
	    }
	}
	
	
	self.sortFacets(checked_objectedItems);
	self.sortFacets(unchecked_objectedItems);
	
	var objectedItems = checked_objectedItems.concat(unchecked_objectedItems);
	
	if (typeof self.init_objectedItems == 'undefined') {
		//	$.extend(cur_facets_hash_copy, cur_facets_hash );
	    var objectedItems_copy = JSON.parse(JSON.stringify(objectedItems));
	    var cur_facets_hash_copy = JSON.parse(JSON.stringify(cur_facets_hash));
	    self.init_objectedItems = objectedItems_copy;
	    self.init_facets_hash = cur_facets_hash_copy;
	}
	
	if (typeof self.max_facets == 'undefined') {
		var num_to_add_from_init = self.init_objectedItems.length;
	    for (var i=0; i < num_to_add_from_init; i++) {
			if (!cur_facets_hash[self.init_objectedItems[i].facet]) {
				objectedItems.push(self.init_objectedItems[i]);
			}
	    }
	}
	var self_target = $(self.target);

	self_target.empty();
	var num_hidden = 0;
	for (var i = 0; i < objectedItems.length; i++) {
	    //      if (typeof this.max_facets != 'undefined') {
	    //	  if (i >= this.max_facets) { break; }
	    //      }
	    var facet = objectedItems[i].facet;
	    var cur_facet_count = (typeof cur_facets_hash[facet] != 'undefined') ? cur_facets_hash[facet].count : 0;
	    
	    var checked_txt = '';
	    if (self.check_state && self.check_state[facet]) {
		checked_txt = ' checked=true';
	    }
	    if ((typeof self.max_show == 'undefined') || (i < self.max_show)) {
		if (cur_facet_count != 0) {
		    self_target.append($(`<input type=checkbox id=" ${self.field} ${facet}_checkbox" ${checked_txt}></input>`)
				       .change(this.checkboxChange(facet))
				      );
		    self_target.append($('<span class="facet-name" ></span>').text(facet));
		    self_target.append($('<span class="facet-count" ></span>').text(' (' + cur_facet_count + ')'));
			self_target.append($('<br>'));
			
		}
	    }
	} // end for loop

	// Hide the field titles when they do not have any content
	var size= $(`#${self.field} input:checkbox`).length;
	if(size === 0){
	    $(`.${self.field}-class`).hide();
	}
	else{
	    $(`.${self.field}-class`).show();
	}
    } // end of AfterRequest

}); // end of extension
