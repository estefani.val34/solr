/*
El método afterRequest anterior recopila todos los valores
de los parámetros
fq utilizando el método API de valores de ParameterStore. 
Para cada valor de parámetro, crea un enlace que muestra el 
valor del parámetro que, cuando se hace clic, elimina el valor
del parámetro (utilizando el método de API ParameterStore
removeByValue) y envía una solicitud a Solr. Si se creó algún
enlace, muestra los enlaces. Realiza una operación similar 
para el parámetro q que presentaremos en el siguiente paso. 
Si no se crearon enlaces, muestra el texto "¡Ver todos los
documentos!"

(El método removeFacet es necesario para evitar los cierres 
de JavaScript).

Por último, agreguemos un enlace para eliminar todos los
filtros actuales. Agregue el siguiente fragmento antes de 
if (links.length) {...:

Si se creó más de un enlace, se crea un enlace que muestra
las palabras "eliminar todo", que, al hacer clic, elimina
todos los parámetros fq (utilizando el método de 
eliminación de API ParameterStore) y envía una solicitud a Solr.
*/

AjaxSolr.CurrentSearchWidget = AjaxSolr.AbstractWidget.extend({
  start: 0,

  afterRequest: function () {
    var self = this;
    var links = [];

    var q = this.manager.store.get('q').val();
    if (q != '*:*') {
      links.push($('<a href="#"></a>').text('(x) ' + q).click(function () {
        self.manager.store.get('q').val('*:*');
        self.doRequest();
        return false;
      }));
    }

    var fq = this.manager.store.values('fq');
    for (var i = 0, l = fq.length; i < l; i++) {
      links.push($('<a href="#"></a>').text('(x) ' + fq[i]).click(self.removeFacet(fq[i])));
    }

    if (links.length > 1) {
      links.unshift($('<a href="#"></a>').text('remove all').click(function () {
        self.manager.store.get('q').val('*:*');
        self.manager.store.remove('fq');
        self.doRequest();
        return false;
      }));
    }

    if (links.length) {
      var $target = $(this.target);
      $target.empty();
      for (var i = 0, l = links.length; i < l; i++) {
        $target.append($('<li></li>').append(links[i]));
      }
    }
    else {
      $(this.target).html('<li>None selected</li>');
    }
  },

  removeFacet: function (facet) {
    var self = this;
    return function () {
      if (self.manager.store.removeByValue('fq', facet)) {
        self.doRequest();
      }
      return false;
    };
  }
});
