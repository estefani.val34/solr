/*
Cree un nuevo widget, TagcloudWidget.js, 
heredando de AbstractFacetWidget:
AbstractFacetWidget proporciona muchas funciones convenientes 
específicas para widgets de facetado, pero también 
puede heredar de AbstractWidget si elige no usar esas funciones.



this.manager.response debería resultarle familiar desde
ResultWidget en el Paso 2. this.field es la propiedad
de campo que establecemos al agregar la instancia de 
widget al administrador. Entonces, en este fragmento, 
estamos inspeccionando
los datos de faceta para ese campo en la respuesta de Solr. 

clickHandler es una de las funciones convenientes proporcionadas
por AbstractFacetWidget. Intenta agregar un parámetro fq 
correspondiente al campo de facetas del widget y el valor de
faceta dado; si tiene éxito, envía una solicitud a Solr.
Para obtener una lista completa de las funciones definidas por
AbstractFacetWidget, consulte la documentación.*/

AjaxSolr.TagcloudWidget = AjaxSolr.AbstractFacetWidget.extend({
    afterRequest: function () {
    var self = this;
    var self_target = $(self.target); 
      if (self.manager.response.facet_counts.facet_fields[self.field] === undefined) {
        self_target.html('no items found in current selection');
        return;
      }

      var maxCount = 0;
      var objectedItems = [];
      for (var facet in self.manager.response.facet_counts.facet_fields[self.field]) {
        var count = parseInt(self.manager.response.facet_counts.facet_fields[self.field][facet]);
        if (count > maxCount) {
          maxCount = count;
        }
        objectedItems.push({ facet: facet, count: count });
      }
      objectedItems.sort(function (a, b) {
        return a.facet < b.facet ? -1 : 1;
      });

      self_target.empty();
      for (var i = 0, l = objectedItems.length; i < l; i++) {
        var facet = objectedItems[i].facet;
        self_target.append(
          $('<span class="tagcloud_item"></span>')
          .text(facet + ' (' + objectedItems[i].count + ')')
          .click(self.clickHandler(facet))
        );
      }
    }
  });
