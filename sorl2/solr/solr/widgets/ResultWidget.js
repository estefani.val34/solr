
/*Visualicemos los documentos en la respuesta de Solr
creando un widget de resultados. Podemos crear un nuevo widget,
ResultWidget.js, heredando de
AbstractWidget, del que hereda cada widget AJAX Solr.


Cada widget toma una identificación requerida, para identificar
el widget, y un objetivo opcional. El objetivo suele ser el
selector CSS para el elemento HTML que el widget actualiza 
después de cada solicitud de Solr.

*/



AjaxSolr.ResultWidget = AjaxSolr.AbstractWidget.extend({
    start: 0,

    beforeRequest: function () {
	$(this.target).html($('<img>').attr('src', 'images/ajax-loader.gif'));
    },

	/*Lo anterior crea enlaces para navegar por tema, 
	organización o intercambio. Al hacer clic en un enlace,
	se restablecerán las consultas de filtro, se agregará una 
	consulta de filtro y se enviará una solicitud de Solr,
	estableciendo el parámetro de inicio de Solr en 0. Vea los
	parámetros de ParameterStore remove y addByValue 
	y AbstractWidget doRequest API.

	Para implementar el enlace "más", implementamos otro método
	abstracto: init. El método init de un widget se llama una 
	vez cuando se llama al método init del Administrador.
	*/

    facetLinks: function (facet_field, facet_values) {
	var links = [];
	if (facet_values) {
	    for (var i = 0, l = facet_values.length; i < l; i++) {
		if (facet_values[i] !== undefined) {
		    links.push(
			$('<a href="#"></a>')
			    .text(facet_values[i])
			    .click(this.facetHandler(facet_field, facet_values[i]))
		    );
		}
		else {
		    links.push('no items found in current selection');
		}
	    }
	}
	return links;
    },

    facetHandler: function (facet_field, facet_value) {
	var self = this;
	return function () {
	    self.manager.store.remove('fq');
	    self.manager.store.addByValue('fq', `${facet_field} : ${AjaxSolr.Parameter.escapeValue(facet_value)}`);
	    self.doRequest(0);
	    return false;
	};
	},
	
	/*Ahora, implementamos el método abstracto afterRequest,
	que cada  widget ejecuta después de que el Administrador 
	recibe la respuesta de Solr. El Administrador almacena
	la respuesta en Manager.response (a la cual los widgets 
	pueden acceder a través de this.manager.response).
	
	El método afterRequest vacía el elemento HTML de destino
	y le agrega contenido HTML para cada documento en la 
	respuesta Solr.

	Nota: Si está utilizando su propia instancia de Solr,
	es posible que deba cambiar el método de la plantilla para
	que coincida con sus propios campos de Solr: específicamente,
	doc.text, doc.dateline, doc.title y doc.id.*/

    afterRequest: function () {
	var this_target = $(this.target);
	this_target.empty();
	if (this.no_init_results) {
	    if ((this.manager.store.get('q').value == '*:*') &&
		(this.manager.store.values('fq').length <= 0)) {
		return;
	    } //Added so initial *:* query doesn't show results
	}
	for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
	    var doc = this.manager.response.response.docs[i];
	    this_target.append(this.template(doc,this.manager.response.highlighting));
	}
    },

    getDocSnippets: function(highlighting, doc) {
	
	var id_val = doc['id']; //Change if your documents have different ID field name
	var cur_doc_highlighting = highlighting[id_val];
	var all_snippets_arr = [];
	if (typeof cur_doc_highlighting != 'undefined') {
	    for (var snip_k in cur_doc_highlighting) {
		var cur_snippets = cur_doc_highlighting[snip_k];
		for (var snip_i=0; snip_i < cur_snippets.length; snip_i++) {
		    var cur_snippet_txt = cur_snippets[snip_i];
		    all_snippets_arr.push(cur_snippet_txt);
		}
	    }
	}
	var cur_doc_snippets_txt = `...${all_snippets_arr.join('...')}...`;
	return(cur_doc_snippets_txt);
    },

    template: function (doc,highlighting) {
	var snippet = '';
	var cur_doc_highlighting_txt;
	if (this.highlighting && highlighting) {
	    cur_doc_highlighting_txt = this.getDocSnippets(highlighting,doc);
	}
	if (!(this.isBlank(cur_doc_highlighting_txt) || /^\s*\.*\s*$/.test(cur_doc_highlighting_txt))) {
	    snippet += cur_doc_highlighting_txt;
	}
	/*
	  snippet += '<br><span style="display:none;"><br><b>FULL DESCRIPTION</b>:' + doc.description[0];
	  snippet += '</span> <a href="#" class="more">more</a>';
	  } else if (doc.description[0].length > 300) {
	  snippet += doc.description[0].substring(0, 300);
	  snippet += '<span style="display:none;">' + doc.description[0].substring(300);
	  snippet += '</span> <a href="#" class="more">more</a>';
	  } */
	else {
	    snippet += doc.description;
	}

	var output = `<h2 id="hiprueba" ><a href=${doc.url} class="search_title" target="_blank">${doc.title}</a></h2>
						<p>${snippet}</p>
						`;
	return output;
    },

    isBlank: function(str) {
	return (!str || /^\s*$/.test(str));
    },

}); // end ResultWidget

