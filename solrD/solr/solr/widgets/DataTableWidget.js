
(function ($) {
    AjaxSolr.DataTableWidget = AjaxSolr.AbstractWidget.extend({
        currentColumns: [],
        currentData: [[]],
        init: function () {
            var self = this; // So we can use the this reference from inner function
            // FIRST BUILD THE COLUMNS from the first row (change this in the future)
            this.currentColumns = [];

            // $.each(self.manager.fields.split(","), function (key, value) {
            //     if (self.currentColumns.length == 0)
            //         self.currentColumns[self.currentColumns.length] = {"id":value, "sType":"html"};
            //     else
            //         self.currentColumns[self.currentColumns.length] = {"id":value};
            // });
            // Initialize data
            var table = $(this.target).DataTable({
                "ordering": true,
                "bSort": true,
                "bJQueryUI": true,
                "bProcessing": true,
                "pagingType": "full_numbers_no_ellipses",
             
                "bServerSide": true,
                "sAjaxSource": null,
                "bLengthChange": true,
                "fnServerData": function (sSource, aoData, fnCallback, settings) {
                    // Rownumber
                    var rows = getSetting(aoData, 'iDisplayLength');
                    self.manager.store.addByValue('rows', parseInt(settings._iDisplayLength));
                    // Filtering
                    // var q = getSetting(aoData, 'sSearch');
                    // if (q.length > 0)
                    //self.manager.store.addByValue('q', 'id:*');
                    // else 
                    self.manager.store.addByValue('q', '*:*');
                    // Fields
                    //self.manager.store.addByValue('fl', self.manager.fields);
                    // Paging
                    //var start = getSetting(aoData, 'iDisplayStart');

                    //self.manager.store.addByValue('start', parseInt(settings.__iDisplayStart));
                    // Sorting
                    var sortcolnr = getSetting(aoData, 'iSortCol_0');


                    var sortcol = "";
                    // if (self.currentColumns[sortcolnr] != undefined)
                    //     sortcol = self.currentColumns[sortcolnr].id;
                    // var sortdir = getSetting(aoData, "sSortDir_0");
                    // if (sortcol.length > 0 && sortdir.length > 0)
                    // self.manager.store.addByValue('bSorted', true);
                    console.log("data")
                    console.log(settings)

                    self.manager.doRequest();
                    self.manager.setCallback(this, fnCallback);


                }, "aoColumns": [
                    {aoData: "id" },
                    { aoData: "title", "defaultContent": "" },

                ]
            });
         

        },
        afterRequest: function () {
           

        }
    });
    function getSetting(aoData, settingName) {
        var returnValue;
        $.each(aoData, function (key, value) {
            if (value.name == settingName) {
                returnValue = value.value;
                return false;
            }
        });
        return returnValue;
    }
})(jQuery);
