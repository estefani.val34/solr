(function (callback) {
    if (typeof define === 'function' && define.amd) {
        define(['core/AbstractManager'], callback);
    }
    else {
        callback();
    }
}(function () {

    AjaxSolr.DataTableManager = AjaxSolr.AbstractManager.extend({
        fields: null,
        datatable: null,
        fnCallback: null,
        convertedData: null,

        executeRequest: function (servlet, string, handler, errorHandler, disableJsonp) {
            var self = this;

            if (this.proxyUrl) {
                jQuery.post(this.proxyUrl, { query: this.store.string() }, function (data) { self.handleResponse(data); }, 'json');
            } else {
                jQuery.getJSON(this.solrUrl + servlet + '?' + this.store.string() + '&wt=json&json.wrf=?', {}, function (data) {
                    self.handleResponse(data);

                    if (typeof self.response.response.docs[0] != 'undefined') {
                        var currentColumns = [];
                        $.each(self.response.response.docs[0], function (key, row) {
                            currentColumns[currentColumns.length] = { "id": key };
                        });

                        // BUILD THE ROWS
                        var currentData = [];
                        $.each(self.response.response.docs, function (key, row) {
                            var newrow = [];
                            $.each(row, function (key, value) {
                                newrow[newrow.length] = value;
                            });
                            currentData[currentData.length] = newrow;
                        });

                    }
                    // COLUMNS


                    self.convertedData = {};
                    self.convertedData["iTotalDisplayRecords"] = self.response.response.numFound;
                    self.convertedData["iTotalRecords"] = self.response.response.numFound;
                    self.convertedData["aaData"] = currentData;
                    self.convertedData["aoColumns"] = currentColumns;
                    self.fnCallback(self.convertedData);
                });
            }
        },
        setCallback: function (dTable, dtFnCallback) {
            this.datatable = dTable;

            this.fnCallback = dtFnCallback;
        }
    });


}));

